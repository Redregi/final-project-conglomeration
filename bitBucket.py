mkdir /path/to/your/project
cd /path/to/your/project
git init
git remote add origin https://Redregi@bitbucket.org/Redregi/week-3.git

echo "Josh Churchill" >> contributors.txt
git add contributors.txt
git commit -m 'Initial commit with contributors'
git push -u origin master

cd /path/to/my/repo
git remote add origin https://Redregi@bitbucket.org/Redregi/week-3.git
git push -u origin --all # pushes up the repo and its refs for the first time
git push -u origin --tags # pushes up any tags


