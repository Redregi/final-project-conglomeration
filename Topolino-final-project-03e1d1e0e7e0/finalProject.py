# Name: Joey Carberry
# Date: September 25, 2015
# Project: Final Project

# List of the functions that happen in the game, that are often called, either at the start of a day, the end of a day,
# or at the point a player makes a choice.

def checkHealth()
    # When: After every choice and after every day.
def endDay()
    # Ends the day and calls all functions related to the end of the day, and the start of a new day.
def gameScreen()
    # Once the player dies, the end screen is displayed. Stats are displayed, including final health, food, water,
    # sanity, kills, people saved, choices made, random events survived, days health has been below 25, ect.
def randomEvent()
    # After the end of the day, in the morning they may be a chance that event occurs. There is a 2-3% that there will
    # be a random event, such as a natural disaster, or a raiding party attacks you. None of these lead to certain
    # death.
def sanityChance()
    # Once a choice is made, this function is called
health = 100
water = 100
food = 100
sanity = 100
'''

!!! All of this code is non-functional and will be worked on later !!!

# Collects health, water, food, and sanity, and displays different message based on what value they are at.
# If the Character has any attributes that reach zero, it will trigger a death and a death message.
if (health == 0 or water == 0 or food == 0 or sanity == 0):
    if (health == 0):
        print ('Game Over. You have died of low health.')
        # stop game and display stats
    elif (water == 0):
        print ('Game Over. You have died of dehydration.'):
        # stop game and display stats
    elif (food == 0):
        print ('Game Over. You have died from starvation.')
        # stop game and display stats
    elif (sanity == 0):
        print ('Game Over. You have succumbed to insanity.'):
        # stop game and display stats
# Character Stats displayed at the end of each day. As the values go lower the character become more and more distressed

# Note for this if statement, it is notable that there is no need to check if the the character is at 0 health because
# the game would have already ended, and all the if statements below would not run.
if (health <= 10 or water <= 10 or food <= 10 or sanity <= 10):
    # randomly generate number. Number correspond to a
if (health <= 20 or water <= 20 or food <= 20 or sanity <= 20):

if (health <= 30 or water <= 30 or food <= 30 or sanity <= 30):

if (health <= 40 or water <= 30 or food <= 30 or sanity <= 30):
def gameOver()
    # Stops all loops and ends the game
'''
