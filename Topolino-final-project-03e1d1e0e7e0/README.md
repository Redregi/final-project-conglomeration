# Cave Adventure Status Report #

### Jan 12 ###
Most of the code was part of the Luum Slime Mold battle. This is the last battle I will add before moving on to adding complexity to the game and polishing the game. I expect that the battle will be finished in another 1-2 hours.

### Jan 13 ###
Nearly finished the dialogue for the Slime Mold battle

### Jan 14 ###
Finished the dialogue and the entire fight's content. Now adding time.sleep() in between the print statements to base my lists on.

### Jan 15 ###
Finished all of the time.sleep statements and had people playtest. Fixed multiple bugs.