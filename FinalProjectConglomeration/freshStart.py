#Josh's Final Project
global x
global y
x = 0
y = 0
menuLoop = true
finSelect = "Nothing"
def menuEntry():
  global finSelect
  defSelect = "Nothing"
  global menuLoop
  print("=============================================")
  print("1. ASCII Image")
  print("2. Pixelize Image")
  print("3. Glitch Image")
  print("4. Randomize Pixels")
  print("5. Picture Pixel Swap")
  print("6. Picture Outline")
  print("7. Picture Merger")
  defSelect = raw_input("Please select a program you'd like to run. For more information on each program type in \"Q\" ")
  if(defSelect == "Q" or defSelect == "q"):
    questionDef()
  if(defSelect == "1" or defSelect == "2" or defSelect == "3" or defSelect == "4" or defSelect == "5" or defSelect == "6" or defSelect == "7"):
    menuLoop = false
    finSelect = defSelect
   
def questionDef():
  print("=============================================")
  print("ASCII Image takes an image, and converts it into greyscale. Once it is converted the program analyzes the darkness of the picture, assigns character values, and re-writes the picture in text form")
  print("")
  print("Pixelize Image takes an image, and pixelizes it based on a user input, which decides how pixelized it becomes")
  print("")
  print("Glitch Image takes an image, and moves around chunks of the image into random positions, making the image looked glitched. How glitched the image becomes is based ona user input")
  print("")
  print("Random Pixels takes an image, and moves around the pixels of the image into random new positions. This program takes a long time, and should only be done with smaller pictures, such as 300 x 300")
  print("")
  print("Picture Pixel Swap takes two images, and takes the pixels of the first image selected, or the largest image, and writes it into a new image under the design of the second, or smaller image. So you can get make the second picture the same hue as the first one. This also takes a long time, and should only be attempted with smaller pictures")
  print("")
  print("Picture Outline takes a picture and makes it look as if it was drawi in black and white")
  print("")
  print("Picture Merger takes two pictures and overlays the larger one onto the smaller one")
  print("")
  back = raw_input("When you are done reading, type \"Done\" into the console to return to the previous screen ")
  if(back == "Done" or "done"):
    defSelect = "Nothing"
    menuEntry()
      
charList = [".",":","`",";","-","~","_","!",")","I","<","^","?","(",">","=","Y","L",'F','1','V','3','X','5','4','P','K','G','O','A','E','Q','8','&','$','W','%']

while menuLoop:
  menuEntry()
  global finSelect

print("Please pick a picture that you'd like to be manipulated")
picFile = pickAFile()
picMan = makePicture(picFile)
picH = getHeight(picMan)
picW = getWidth(picMan)

print("Please pick a folder that you'd like your manipulated picture to be saved to")
picPath = setMediaPath()
newPicture = raw_input("Please enter the name you'd like for the file ")
finPath = picPath + newPicture + ".png"

from random import randint
import math

#------------------------------------------------------------------
#Picture Merger
def picMerger():
  print("Please pick the second picture you'd like to use")
  picFileTwo = pickAFile()
  picManTwo = makePicture(picFileTwo)
  picTH = getHeight(picManTwo)
  picTW = getWidth(picManTwo)
  pOneD = picH * picW
  pTwoD = picTH * picTW
  if pOneD > pTwoD:
    pNewH = picTH
    pNewW = picTW
  else:
    pNewH = picH
    pNewW = picW
  mergedPic = makeEmptyPicture(pNewW, pNewH, white)
  mergedPix = getPixels(mergedPic)
  d = float(pNewH * pNewW)
  c = 0.0
  percentList = list()
  for p in mergedPix:
    percent = round((c/d)*100)
    if percent in percentList:
      nothing = "Do nothing"
    else:
      print("Loading... ", str(percent), "Percent")
      percentList.append(percent)
    x = getX(p)
    y = getY(p)
    pOnePix = getPixel(picMan, x, y)
    pTwoPix = getPixel(picManTwo, x, y)
    rOne = getRed(pOnePix)
    gOne = getGreen(pOnePix)
    bOne = getBlue(pOnePix)
    rTwo = getRed(pTwoPix)
    gTwo = getGreen(pTwoPix)
    bTwo = getBlue(pTwoPix)
    r = (rOne + rTwo)/2
    g = (gOne + gTwo)/2
    b = (bOne + bTwo)/2
    finColor = makeColor(r,g,b)
    setColor(p, finColor)
    c += 1
  show(mergedPic)
  writePictureTo(mergedPic, str(finPath))

#------------------------------------------------------------------
#Outline Picture
def picOutline():
  picPixels = getPixels(picMan)
  outlinePic = makeEmptyPicture(picW, picH, white)
  colList = list()
  c = 0
  percentCount = 0
  percentSet = list()
  d = float(picH * picW)
  for p in picPixels:
    percent = (round(100*(float(percentCount/d))))
    if percent in percentSet:
      nothing = "Do nothing"
    else:
      percentSet.append(percent)
      print("Loading... ", str(percent), "Percent")
    r = getRed(p)
    g = getGreen(p)
    b = getBlue(p)
    color = makeColor(r,g,b)
    colList.append(color)
    c += 1 
    if c == 2:
      colDif = distance(colList[0], colList [1])
      if colDif >= 5:
        x = getX(p)
        y = getY(p)
        newPix = getPixel(outlinePic, x, y)
        setColor(newPix, black)
        #print("True Two")
      colList = list()
      pixList = list()
      c = 0
    percentCount += 1
  print("Your swapped picture is now complete! Thank you for using CB Technologies!")
  show(outlinePic)
  writePictureTo(outlinePic, str(finPath))
      
      
#------------------------------------------------------------------
#Swap Pixels 
def picSwap():
  print("Please pick the picture that you'd like to work with")
  picFileTwo = pickAFile()
  picManTwo = makePicture(picFileTwo)
  picTH = getHeight(picManTwo)
  picTW = getHeight(picManTwo)
  if(picTH*picTW > picH*picW):
    picFH = picH
    picFW = picW
    bigPic = picManTwo
    smallPic = picMan
  if(picTH*picTW < picH*picW):
    picFH = picTH
    picFW = picTW
    bigPic = picMan
    smallPic = picManTwo
  if(picTH*picTW == picH*picW):
    picFH = picH
    picFW = picW
    bigPic = picManTwo
    smallPic = picMan
  swapPic = makeEmptyPicture(picFW, picFH, white)
  bigPix = getPixels(bigPic)
  smallPix = getPixels(smallPic)
  colDict = {}
  bigColor = set()
  bigRGB = set()
  for sB in bigPix:
    colList = list()
    r = getRed(sB)
    g = getGreen(sB)
    b = getBlue(sB)
    rS = math.pow(r,2)
    gS = math.pow(g,2)
    bS = math.pow(b,2)
    colFunction = math.sqrt(rS+gS+bS)
    colList.append(r)
    colList.append(g)
    colList.append(b)
    colDict[colFunction] = colList
  h = getHeight(smallPic)
  w = getWidth(smallPic)
  d = float(h*w)
  percentCount = 0
  percentSet = list()
  for sP in smallPix:
    percent = (round(100*(float(percentCount/d))))
    if percent in percentSet:
      nothing = "Do nothing"
    else:
      percentSet.append(percent)
      print("Loading... ", str(percent), "Percent")
    r = getRed(sP)
    g = getGreen(sP)
    b = getBlue(sP)
    rS = math.pow(r,2)
    gS = math.pow(g,2)
    bS = math.pow(b,2)
    colFunction = math.sqrt(rS+gS+bS)
    smallCol = getColor(sP)
    x = getX(sP)
    y = getY(sP)
    swapPix = getPixel(swapPic, x, y)
    if(colFunction in bigColor):
      setColor(swapPix, smallCol)
    else:
      diffColors = list([(key - colFunction) for key in colDict])
      absColors = list()
      for el in diffColors:
        el = abs(el)
        absColors.append(el)
      small = min(absColors)
      pos = absColors.index(small)
      finCol = diffColors[pos] + colFunction
      if finCol > 109 and finCol < 110:
        setColor(swapPix, smallCol)
      else:
        r = colDict[finCol][0]
        g = colDict[finCol][1]
        b = colDict[finCol][2]
        finCol = makeColor(r,g,b)
        setColor(swapPix, finCol)
    percentCount += 1
  print("Your swapped picture is now complete! Thank you for using CB Technologies!")
  show(swapPic)
  writePictureTo(swapPic, str(finPath))

#------------------------------------------------------------------
#Picture McJumbler
def mcJumbler():
  mcJumbled = makeEmptyPicture(picW, picH, white)
  coordinates = list()
  original = getPixels(picMan)
  jumbled = getPixels(mcJumbled)
  c = 0
  h = getHeight(picMan)
  w = getWidth(picMan)
  d = float(h*w)
  percentSet = list()
  for p in jumbled:
    percent = (round(100*(float(c/d))))
    if percent in percentSet:
      nothing = "Do nothing"
    else:
      percentSet.append(percent)
      print("Loading... ", str(percent), "Percent")
    index = randint(0, len(original)-1)
    randPixel = original[index]
    randColor = getColor(randPixel)
    setColor(p, randColor)
    original.remove(randPixel)
    c += 1
  print("Your jumbled picture is now complete! Thank you for using CB Technologies!")
  show(mcJumbled)
  writePictureTo(mcJumbled, str(finPath))
  
#------------------------------------------------------------------
#Glitched
def glitchBasic():
  global gHeight
  global randHeight
  global glitchAmount
  global gLocation 
  global pushGlitch
  gHeight = int(picH / 70)
  randHeight = gHeight * randint(1,10)
  gLocation = randint(0, picH-randHeight)
  pushGlitch = randint(10, 100) 
  
def roundBackground(picMan):
  global chunkColor
  print("Your picture is now being analyzed, this shouldn't take long")
  amount = getPixels(picMan)
  r = 0.0
  g = 0.0
  b = 0.0
  c = 0
  for p in amount:
    r += getRed(p)
    g += getGreen(p)
    b += getBlue(p)
    c += 1
  chunkR = int(round(r / c))
  chunkG = int(round(g / c))
  chunkB = int(round(b / c))
  chunkColor = makeColor(chunkR, chunkG, chunkB)

def glitchFull():
  global glitchPic
  roundBackground(picMan)
  numInput = int(input("How glitched would you like your image to be on a scale of 1-100? It will take longer to load the larger the number you select "))
  glitchPic = makeEmptyPicture(picW+100, picH, white)
  addRectFilled(glitchPic, 0, 0, picW+100, picH, chunkColor)
  copyInto(picMan, glitchPic, 0, 0)
  print("Your picture is now being manipulated, thank you for your patience")
  #glitchAmount = randint(5,20)
  percentSet = list()
  for a in range(0,numInput):
    glitchBasic()
    glitchChunk = makeEmptyPicture(picW, randHeight, white)
    function = round(100*(float(a*1.0)/numInput))
    print(function)
    if function in percentSet:
      nothing = "Do nothing"
    else:
      print(str("Loading..."), function, str("Percent"))
      percentSet.append(function)
    for y in range(gLocation, gLocation + randHeight):
      #print(gLocation, gLocation + randHeight)
      for x in range(0, picW):
        originPixel = getPixel(picMan, x, y)
        originColor = getColor(originPixel)
        newPixel = getPixel(glitchChunk, x, y - gLocation)
        setColor(newPixel, originColor)
    #print(glitchChunk)
    #print(glitchPic)
    #print(100+pushGlitch)
    addRectFilled(glitchPic, 0, gLocation, pushGlitch, randHeight, chunkColor)
    copyInto(glitchChunk, glitchPic,pushGlitch, gLocation)
    addRectFilled(glitchPic, picW + pushGlitch, gLocation, 100-pushGlitch, randHeight, chunkColor)
  print("Your glitched picture is now complete! Thank you for using CB Technologies!")
  show(glitchPic)
  writePictureTo(glitchPic, str(finPath))

#------------------------------------------------------------------
#Pixelized
def colorAnalyze(picMan):
  r = 0.0
  g = 0.0
  b = 0.0
  for y in range(bY, 12 + bY):
    for x in range(aX, 11 + aX):
      pixSl = getPixel(picMan, x, y)
      r += getRed(pixSl)
      g += getGreen(pixSl)
      b += getBlue(pixSl)
  chunkR = int(round(r / chunkSize))
  chunkG = int(round(g / chunkSize))
  chunkB = int(round(b / chunkSize))
  chunkColor = makeColor(chunkR, chunkG, chunkB)
  for n in range(0, 4 + (chunkInput/2) ):
    chunkColor = makeLighter(chunkColor)
  addRectFilled(pixelC, aX, bY, pWidth, pHeight, chunkColor)
   
def pixelChunk(picMan):
  global a
  global b
  global aX
  global bY
  global chunkSize
  global pixelC
  global pHeight
  global pWidth
  global chunkInput
  a = 0 
  b = 0
  aX = a + x 
  bY = b + y 
  pixelC = makeEmptyPicture(picW, picH, white)
  chunkInput = float(input("How pixelated do you want your picture to be on a scale of 1-10? One being not very pixelized and ten being extremely pixelized."))
  print("Your picture is now being pixelized, thank you for your patience")
  pixelation = ((10 - chunkInput)*5) + 50
  pHeight = int(picH / pixelation)
  pWidth = int(picW / pixelation)
  chunkSize = pHeight * pWidth
  percentSet = list()
  for b in range(0, picH - pHeight, pHeight):
    aX = 0
    percent = round((float(b*1.0)/picH)*100)
    if percent in percentSet:
      nothing = "Do nothing"
    else:
      print(str("Loading..."), percent, str("Percent"))
      percentSet.append(percent)
    for a in range(0, picW - pWidth, pWidth):
      colorAnalyze(picMan)
      aX += pWidth
    bY += pHeight
  print("Your pixelized picture is now complete! Thank you for using CB technologies!")
  show(pixelC)
  writePictureTo(pixelC, str(finPath))
  
  
  
#------------------------------------------------------------------
#ASCII
def convertBW(picMan):
  print("Your picture is currently being converted into greyscale, thank you for your patience as this will take longer with larger picture files")
  for p in getPixels(picMan):
    intensity = (getRed(p) + getGreen(p) + getBlue(p)) / 3
    setColor(p,makeColor(intensity, intensity, intensity))
  return(picMan)
  
def chunkPercent(bwPic):
  c = 0.0
  cA = 1.0
  for y in range(bY, 12 + bY): #Y in range chunkY + y
    for x in range(aX, 11 + aX):# X in range chunkX + x
      pixSl = getPixel(bwPic, x, y)
      r = getRed(pixSl)
      g = getGreen(pixSl)
      b = getBlue(pixSl)
      for u in range(0, r+1):
        if(r == u):
          c += cA
        cA -= .0039215686
      cA = 1.0 
  percent = c / 132.0
  cL = 0
  cP = percent + .027027027
  l = 0.0
  t = 0
  for w in range(0,37):
    #print(percent, w, l)
    if(l >= percent and l < cP):
      addText(ASCII, aX, bY, charList[w])
      t += 1
      #print(charList[cL])
    l += .027027027
    if(w == 36 and t == 0):
      addText(ASCII, aX, bY, charList[w])
      #print(charList[w])
      
def chunkMove(bwPic):
  print("Greyscale... done")
  print("Your picture is currently being converted into ASCII, thank you for your patience")
  global a
  global b
  global aX
  global bY
  global ASCII
  ASCII = makeEmptyPicture(picW, picH, white)
  b = 0
  a = 0
  aX = a + x
  bY = b + y
  percentSet = list()
  for b in range(0, picH - 12, 12):
    lFunction = float(b*1.0)/ picH
    lPercent = round(lFunction * 100.0)
    aX = 0
    if lPercent in percentSet:
      nothing = "Do nothing"
    else:
      print(str("Loading..."), lPercent, str("Percent"))
      percentSet.append(lPercent)
    for a in range(0, picW - 11, 11):
      chunkPercent(bwPic)
      aX += 11
    bY += 12
  print("Your ASCII picture is now complete! Thank you for using CB technologies!")
  show(ASCII)
  writePictureTo(ASCII, str(finPath))

def ASCII(): 
  bwPic = convertBW(picMan)
  chunkMove(bwPic)
#------------------------------------------------------------------

if finSelect == "1":
  ASCII()
if finSelect == "2":
  pixelChunk(picMan)
if finSelect == "3":
  glitchFull()
if finSelect == "4":
  mcJumbler()
if finSelect == "5":
  picSwap()
if finSelect == "6":
  picOutline()
if finSelect == "7":
  picMerger()


