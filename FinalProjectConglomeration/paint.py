#Josh Churchill]
#Paint
from tkinter import *
from random import *
CusColor = "white"
window = Tk()
menu = Toplevel()
window.title("JC Paint")
menu.title("Paint Menu")
frame = Frame(window, width = 500, height = 500)
emptyCanvas = Canvas(frame, width = 500, height = 500, bg = "white")
menuFrame = Frame(menu, width = 250, height = 95 )
menuFrame.pack()
menuCanvas = Canvas(menuFrame, width = 250, height = 200, bg = "grey")
string = StringVar()
menuCanvas.create_window(100,100)
menuCanvas.create_oval(10,10,30,30, tag = "Red", fill = "red")
menuCanvas.create_oval(30,10,50,30, tag = "Orange", fill = "orange")
menuCanvas.create_oval(50,10,70,30, tag = "Yellow", fill = "yellow")
menuCanvas.create_oval(70,10,90,30, tag = "Green", fill = "green")
menuCanvas.create_oval(90,10,110,30, tag = "Blue", fill = "blue")
menuCanvas.create_oval(110,10,130,30, tag = "Purple", fill = "purple")
menuCanvas.create_oval(130,10,150,30, tag = "Violet", fill = "violet")
menuCanvas.create_oval(150,10,170,30, tag = "Black", fill = "black")
menuCanvas.create_oval(170,10,190,30, tag = "White", fill = "white")
menuCanvas.create_oval(10,30,30,50, tag = "Eraser")
menuCanvas.create_oval(30,30,50,50, tag = "Pen")
menuCanvas.create_oval(211,62,231,82, tag = "CusColor", fill = CusColor)
menuCanvas.create_oval(211,112,231,132, tag = "Save")
color = "black"

def enterClick(event):
    global colorStr
    global CusColor
    CusColor = colorStr.get()
    menuCanvas.create_oval(211,62,231,82, tag = "CusColor", fill = CusColor)

def saveEnter(event):
    global saveStr
    global cusSave
    cusSave = ""
    cusSave += saveStr.get() + "newImage.ps"




eraserImage = PhotoImage(file = "eraser.gif" )
menuCanvas.create_image(20,41, image = eraserImage)
penImage = PhotoImage(file = "pen.gif" )
menuCanvas.create_image(40,41, image = penImage)
saveImage = PhotoImage(file = "save.gif" )
menuCanvas.create_image(221,122, image = saveImage)
menuCanvas.pack()
colorStr = StringVar()
entryBox = Entry(menuCanvas, textvariable = colorStr, width = 10).place(x = 110, y = 58)

saveStr = StringVar()
saveBox = Entry(menuCanvas, textvariable = saveStr, width = 10)
saveBox.place(x = 110, y = 108)
saveBox.bind('<Return>',saveEnter)

Label(menuCanvas, text = "Custom Color", bg = "grey").place(x = 10, y = 60)
Label(menuCanvas, text = "Save Location", bg = "grey").place(x = 10, y = 110)




#entryBox.place(x = 10, y = 60)
mX = 0
mY = 0

drawingTool = 0


def mouseTrack(event):
    global color
    global mX
    global mY
    global drawingTool
    if drawingTool == 0:
        emptyCanvas.create_line(mX,mY,event.x,event.y, tag = "Line", fill = color)
    if drawingTool == 1:
        emptyCanvas.create_oval(event.x,event.y,event.x+10,event.y+10, tag = "Line", fill = "white", outline = "white")
    mX = event.x
    mY = event.y

def mouseClick(event):
    emptyCanvas.bind("<B1-Motion>",mouseTrack)
    global mX
    global mY
    mX = event.x
    mY = event.y

def menuClick(event):
    global color
    global mX
    global mY
    global drawingTool
    global cusColor
    global cusSave
    if(abs((event.x) - 20) <= 10):
        if(abs((event.y)- 20) <= 10):
            color = "red"
    if(abs((event.x) - 40) <= 10):
        if(abs((event.y)- 20) <= 10):
            color = "orange"
    if(abs((event.x) - 60) <= 10):
        if(abs((event.y)- 20) <= 10):
            color = "yellow"
    if(abs((event.x) - 80) <= 10):
        if(abs((event.y)- 20) <= 10):
            color = "green"
    if(abs((event.x) - 100) <= 10):
        if(abs((event.y)- 20) <= 10):
            color = "blue"
    if(abs((event.x) - 120) <= 10):
        if(abs((event.y)- 20) <= 10):
            color = "purple"
    if(abs((event.x) - 140) <= 10):
        if(abs((event.y)- 20) <= 10):
            color = "violet"
    if(abs((event.x) - 160) <= 10):
        if(abs((event.y)- 20) <= 10):
            color = "black"
    if(abs((event.x) - 180) <= 10):
        if(abs((event.y)- 20) <= 10):
            color = "white"
    if(abs((event.x) - 20) <= 10):
        if(abs((event.y)- 40) <= 10):
            drawingTool = 1
    if(abs((event.x) - 40) <= 10):
        if(abs((event.y)- 40) <= 10):
            drawingTool = 0
    if(abs((event.x) - 221) <= 10):
        if(abs((event.y)- 72) <= 10):
            color = CusColor
    if(abs((event.x) - 221) <= 10):
        if(abs((event.y)- 122) <= 10):
            menuCanvas.update()
            emptyCanvas.postscript(file = cusSave,colormode = "color")
            print("Image Saved")

    mX = event.x
    mY = event.y

emptyCanvas.bind("<Button-1>", mouseClick)
menuCanvas.bind("<Button-1>", menuClick)
menu.bind("<Return>", enterClick)


frame.pack()
emptyCanvas.pack()
menu.mainloop()
window.mainloop()